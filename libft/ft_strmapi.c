/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/22 18:48:10 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/22 18:53:12 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int			size;
	int			i;
	char		*res;
	const char	*p;

	if (s)
	{
		p = s;
		size = ft_strlen((char *)s) + 1;
		res = (char *)malloc(sizeof(char) * size);
		if (res)
		{
			res[size - 1] = '\0';
			i = 0;
			while (*p != '\0')
			{
				res[i] = (*f)((unsigned int)i, *p++);
				++i;
			}
			return (res);
		}
	}
	return (NULL);
}
