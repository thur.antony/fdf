/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 13:12:06 by avykhova          #+#    #+#             */
/*   Updated: 2018/01/04 15:13:36 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	size_t	i;

	i = ft_strlen(s);
	if (c == 0)
		return (&((char *)s)[i]);
	if (i == 0)
		return (NULL);
	while (i--)
		if (((char *)s)[i] == (char)c)
			return (&((char *)s)[i]);
	return (NULL);
}
