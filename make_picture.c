/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_picture.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 14:13:11 by avykhova          #+#    #+#             */
/*   Updated: 2018/04/25 14:13:13 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			make_grid(t_data *dt)
{
	int			i;
	int			size_all;
	int			size_x;

	i = -1;
	size_x = dt->c.size[0];
	size_all = dt->c.size[2];
	while (++i < size_all)
	{
		if ((i + size_x) < size_all)
			line(dt->w.all[i], dt->w.all[i + size_x], dt);
		if (((i + 1) % dt->c.size[0]) != 0)
			line(dt->w.all[i], dt->w.all[i + 1], dt);
	}
}

static void		rotate_x(t_data *dt, int size_all, int i)
{
	double		y;
	double		z;

	while (++i < size_all)
	{
		y = dt->w.all[i].y * cos(dt->anglx) - dt->w.all[i].z * sin(dt->anglx);
		z = dt->w.all[i].y * sin(dt->anglx) + dt->w.all[i].z * cos(dt->anglx);
		dt->w.all[i].y = y;
		dt->w.all[i].z = z;
	}
}

static void		rotate_y(t_data *dt, int size_all, int i)
{
	double		x;
	double		z;

	while (++i < size_all)
	{
		x = dt->w.all[i].x * cos(dt->angly) + dt->w.all[i].z * sin(dt->angly);
		z = dt->w.all[i].z * cos(dt->angly) - dt->w.all[i].x * sin(dt->angly);
		dt->w.all[i].x = x;
		dt->w.all[i].z = z;
	}
}

static int		rotate_z(t_data *dt, int size_all, int i)
{
	double		x;
	double		y;

	while (++i < size_all)
	{
		x = dt->w.all[i].x * cos(dt->anglz) - dt->w.all[i].y * sin(dt->anglz);
		y = dt->w.all[i].x * sin(dt->anglz) + dt->w.all[i].y * cos(dt->anglz);
		dt->w.all[i].x = x;
		dt->w.all[i].y = y;
	}
	return (-1);
}

void			make_picture(t_data *dt)
{
	int			i;
	int			center_x;
	int			center_y;
	int			size_all;

	i = -1;
	size_all = dt->c.size[2];
	center_x = (dt->c.all[dt->c.size[0] - 1].x * dt->zoom) / 2;
	center_y = (dt->c.all[dt->c.size[2] - 1].y * dt->zoom) / 2;
	while (++i < size_all)
	{
		dt->w.all[i].x = dt->c.all[i].x * dt->zoom - center_x;
		dt->w.all[i].y = dt->c.all[i].y * dt->zoom - center_y;
		dt->w.all[i].z = dt->c.all[i].z * dt->zoom;
	}
	rotate_x(dt, size_all, -1);
	rotate_y(dt, size_all, -1);
	i = rotate_z(dt, size_all, -1);
	add_offset_helper(dt);
	ft_bzero(dt->image, IMG_LEN);
	make_grid(dt);
	mlx_put_image_to_window(dt->mlx_ptr, dt->win_ptr, dt->img_ptr, -2, -2);
	string_put(dt);
}
