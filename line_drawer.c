/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_drawer.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/01 15:27:21 by avykhova          #+#    #+#             */
/*   Updated: 2018/04/01 15:27:22 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static char		color_calc(t_data *dt, t_coord a, t_coord b)
{
	int			z_min_ab;
	int			z_delta_ab;

	(a.z_init < b.z_init) ? (z_min_ab = a.z_init) : \
	(z_min_ab = b.z_init);
	z_delta_ab = abs(a.z_init - b.z_init);
	return ((char)(z_min_ab * dt->step + z_delta_ab * dt->step));
}

static void		img_pix_put(t_data *dt, t_coord p, char color)
{
	int			byte;

	if (p.x > WIN_WIDTH || p.x < 0 || p.y > WIN_HEIGHT || p.y < 0)
		return ;
	byte = (p.x * dt->bpp) + (p.y * dt->size_line);
	dt->image[byte] = 85 + color;
	dt->image[++byte] = 30 + color;
	dt->image[++byte] = 60 + color;
}

static void		y_draw(t_coord a, t_coord b, t_delta d, t_data *dt)
{
	int			x_incr;
	int			p;

	x_incr = 1;
	if (d.x < 0)
		x_incr = -1;
	img_pix_put(dt, a, color_calc(dt, a, b));
	p = d.xx - d.y;
	while (a.y++ < b.y)
	{
		if (p < 0)
			img_pix_put(dt, a, color_calc(dt, a, b));
		else
		{
			a.x += x_incr;
			img_pix_put(dt, a, color_calc(dt, a, b));
			p -= d.yy;
		}
		p += d.xx;
	}
}

static void		x_draw(t_coord a, t_coord b, t_delta d, t_data *dt)
{
	int			x_incr;
	int			p;

	x_incr = 1;
	if (d.x < 0)
		x_incr = -1;
	img_pix_put(dt, a, color_calc(dt, a, b));
	p = d.yy - d.abs_x;
	while (a.x != b.x)
	{
		a.x += x_incr;
		if (p < 0)
			img_pix_put(dt, a, color_calc(dt, a, b));
		else
		{
			++a.y;
			img_pix_put(dt, a, color_calc(dt, a, b));
			p -= d.xx;
		}
		p += d.yy;
	}
}

void			line(t_coord a, t_coord b, t_data *dt)
{
	t_delta		d;

	if (a.y > b.y)
		line(b, a, dt);
	else
	{
		d.x = b.x - a.x;
		d.y = b.y - a.y;
		d.abs_x = abs(d.x);
		d.yy = d.y << 1;
		d.xx = d.abs_x << 1;
		if (d.y > d.abs_x)
			y_draw(a, b, d, dt);
		else
			x_draw(a, b, d, dt);
	}
}
