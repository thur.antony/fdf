/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_extractor.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 14:40:52 by avykhova          #+#    #+#             */
/*   Updated: 2018/03/26 14:40:54 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		string_to_coord(char **p, int i, t_data *dt)
{
	i = -1;
	dt->c.all = (t_coord *)malloc(sizeof(t_coord) * dt->c.size[2]);
	dt->w.all = (t_coord *)malloc(sizeof(t_coord) * dt->c.size[2]);
	while (++i < dt->c.size[2])
	{
		dt->c.all[i].x = i % dt->c.size[0];
		dt->c.all[i].y = i / dt->c.size[0];
		dt->c.all[i].z = ft_atoi(p[i]);
		dt->w.all[i].z_init = dt->c.all[i].z;
		if (dt->c.all[i].z > dt->z_max)
			dt->z_max = dt->c.all[i].z;
		free(p[i]);
	}
	free(p);
}

int				validator(char *s)
{
	int			fd;
	char		tmp[2];

	if (ft_strstr(s, ".fdf") == NULL)
		exit(1);
	if ((fd = open(s, O_RDWR)) == -1 || read(fd, tmp, 1) <= 0)
		exit(1);
	close(fd);
	return (open(s, O_RDONLY));
}

void			map_extractor(char *s, t_data *dt)
{
	char			*map_line;
	char			*tmp;
	int				fd_init[2];
	char			**points_data;

	tmp = ft_strnew(0);
	fd_init[1] = 0;
	fd_init[0] = validator(s);
	while (get_next_line(fd_init[0], &map_line) > 0)
	{
		if (fd_init[1]++ == 0)
			dt->c.size[0] = ft_words_count(map_line, ' ');
		else if (ft_words_count(map_line, ' ') != dt->c.size[0])
			exit(1);
		dt->c.size[1]++;
		tmp = ft_strjoin(tmp, map_line);
		if (tmp[ft_strlen(tmp) - 1] != ' ')
		{
			tmp = ft_strjoin(tmp, " ");
		}	
		free(map_line);
	}
	close(fd_init[0]);
	dt->c.size[2] = dt->c.size[0] * dt->c.size[1];
	points_data = ft_strsplit(tmp, ' ');
	free(tmp);
	return (string_to_coord(points_data, -1, dt));
}
