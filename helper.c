/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 14:12:39 by avykhova          #+#    #+#             */
/*   Updated: 2018/04/25 14:12:41 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		add_offset_helper(t_data *dt)
{
	int		size_all;
	int		i;

	i = -1;
	size_all = dt->c.size[2];
	while (++i < size_all)
	{
		dt->w.all[i].x += dt->offset_x;
		dt->w.all[i].y += dt->offset_y;
	}
}

void		string_put(t_data *dt)
{
	mlx_string_put(dt->mlx_ptr, dt->win_ptr, 10, 10, WHITE, dt->name);
	mlx_string_put(dt->mlx_ptr, dt->win_ptr, 10, 50, WHITE, "Controls:");
	mlx_string_put(dt->mlx_ptr, dt->win_ptr, 10, 80, WHITE, \
		"move around: arrows");
	mlx_string_put(dt->mlx_ptr, dt->win_ptr, 10, 110, WHITE, \
		"rotations: rigth number's pad");
	mlx_string_put(dt->mlx_ptr, dt->win_ptr, 10, 140, WHITE, \
		"zoom: +/- buttons");
	mlx_string_put(dt->mlx_ptr, dt->win_ptr, 10, 170, WHITE, \
		"exit: ESC");
}
