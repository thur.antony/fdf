/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_extractor.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/26 16:52:15 by avykhova          #+#    #+#             */
/*   Updated: 2018/04/26 16:52:17 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdio.h>

static int		validator(char *s)
{
	int			fd;
	char		tmp[2];

	if (ft_strstr(s, ".fdf") == NULL)
		exit(1);
	if ((fd = open(s, O_RDWR)) == -1 || read(fd, tmp, 1) <= 0)
		exit(1);
	close(fd);
	return (open(s, O_RDONLY));
}

static void		map_extractor_r(t_data *dt, int fd, int i)
{
	char		*map_line;
	char		**pd;

	map_line = NULL;
	pd = NULL;
	while (++i < dt->c.size[2])
	{
		if (!(i % dt->c.size[0]))
		{
			F(map_line);
			if (get_next_line(fd, &map_line) > 0)
			{
				F(pd);
				pd = ft_strsplit(map_line, ' ');
			}
		}
		dt->c.all[i].x = i % dt->c.size[0];
		dt->c.all[i].y = i / dt->c.size[0];
		dt->c.all[i].z = ft_atoi(pd[dt->c.all[i].x]);
		dt->w.all[i].z_init = dt->c.all[i].z;
		if (dt->c.all[i].z > dt->z_max)
			dt->z_max = dt->c.all[i].z;
		free(pd[dt->c.all[i].x]);
	}
	close(fd);
}

void			map_extractor(char *s, t_data *dt)
{
	char			*map_line;
	int				fd_init[2];
	int				fd;

	fd_init[1] = 0;
	fd_init[0] = validator(s);
	while (get_next_line(fd_init[0], &map_line) > 0)
	{
		if (fd_init[1]++ == 0)
			dt->c.size[0] = ft_words_count(map_line, ' ');
		else if (ft_words_count(map_line, ' ') != dt->c.size[0])
			exit(1);
		dt->c.size[1]++;
		free(map_line);
	}
	fd = open(s, O_RDONLY);
	close(fd_init[0]);
	dt->c.size[2] = dt->c.size[0] * dt->c.size[1];
	dt->c.all = (t_coord *)malloc(sizeof(t_coord) * dt->c.size[2]);
	dt->w.all = (t_coord *)malloc(sizeof(t_coord) * dt->c.size[2]);
	return (map_extractor_r(dt, fd, -1));
}
