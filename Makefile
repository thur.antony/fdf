# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: avykhova <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/26 14:41:25 by avykhova          #+#    #+#              #
#    Updated: 2018/03/26 14:41:27 by avykhova         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf
LIBFT_NAME = ./libft/libft.a

SRC_FDF = ./main.c \
	./map_extractor_leaked.c \
	./line_drawer.c \
	./make_picture.c \
	./memory_cleaner.c \
	./helper.c


SRC_EOS = ./manipula_eos.c
SRC_IMAC = ./manipula_imac.c
OBJECT_FDF = $(SRC_FDF:.c=.o) $(SRC_EOS:.c=.o) $(SRC_IMAC:.c=.o)
FLAGS = -Wall -Wextra -Werror -Iincludes/
FLAGS_MLX = -lmlx -framework OpenGL -framework AppKit
FLAGS_MLX_ELEMENTARY = -lmlx -lXext -lX11 -lm -L ./libmlx/ 
FLAGS_LIBFT = -L./libft -lft

all: $(NAME)
	@echo '> compiling a project for iMac'
	@gcc -o $(NAME) $(FLAGS) $(SRC_FDF) $(SRC_IMAC) $(FLAGS_LIBFT) $(FLAGS_MLX)

eos: $(NAME)
	@echo '> compiling a project for elementary OS'
	@gcc -o $(NAME) $(FLAGS) $(SRC_FDF) $(SRC_EOS) $(FLAGS_LIBFT) $(FLAGS_MLX_ELEMENTARY)

libft:
	@echo '> recompiling library libft'
	@make -C ./libft/ re

$(NAME): $(LIBFT_NAME) $(OBJECT_FDF)
	
$(LIBFT_NAME):
	@echo '> recompiling an cleaning library libft'
	@make -C ./libft/ re
	@make -C ./libft/ clean	

%.o: %.c
	@echo '> updating changes in .c files'
	@gcc $(FLAGS) -o $@ -c $<
clean:
	@echo '> cleaning a project'
	@/bin/rm -f $(OBJECT_FDF)
fclean: clean
	@echo '> removing a project'
	@/bin/rm -f $(NAME)
	@make -C ./libft/ fclean
re:	fclean all
