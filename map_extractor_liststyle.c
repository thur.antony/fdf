/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_extractor.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/26 11:57:21 by avykhova          #+#    #+#             */
/*   Updated: 2018/04/26 11:57:23 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdio.h>

static void		string_to_coord(t_list *h, t_data *dt, int i, char **pd)
{
	t_list 		*f;

	dt->c.all = (t_coord *)malloc(sizeof(t_coord) * dt->c.size[2]);
	dt->w.all = (t_coord *)malloc(sizeof(t_coord) * dt->c.size[2]);
	pd = ft_strsplit(h->content, ' ');
	while (++i < dt->c.size[2])
	{
		if (i != 0 && i % dt->c.size[0] == 0 && h->next != NULL)
		{
			if (pd[(i - 1) % dt->c.size[0] + 1])
				free(pd[(i - 1) % dt->c.size[0] + 1]);
			f = h;
			h = h->next;
			free(f->content);
			free(f);
			free(pd);
			pd = ft_strsplit(h->content, ' ');
		}
		dt->c.all[i].x = i % dt->c.size[0];
		dt->c.all[i].y = i / dt->c.size[0];
		dt->c.all[i].z = ft_atoi(pd[i % dt->c.size[0]]);
		dt->w.all[i].z_init = dt->c.all[i].z;
		if (dt->c.all[i].z > dt->z_max)
			dt->z_max = dt->c.all[i].z;
		free(pd[i % dt->c.size[0]]);
	}
	free(pd);
	system("leaks fdf");
}

static int		validator(char *s)
{
	int			fd;
	char		tmp[2];

	if (ft_strstr(s, ".fdf") == NULL)
		exit(1);
	if ((fd = open(s, O_RDWR)) == -1 || read(fd, tmp, 1) <= 0)
		exit(1);
	close(fd);
	return (open(s, O_RDONLY));
}

void			map_extractor(char *s, t_data *dt)
{
	char			*map_line;
	t_list			*head;
	int				fd_init[2];
	char			**points_dt;

	fd_init[1] = 0;
	fd_init[0] = validator(s);
	head = NULL;
	while (get_next_line(fd_init[0], &map_line) > 0)
	{
		if (fd_init[1]++ == 0)
			dt->c.size[0] = ft_words_count(map_line, ' ');
		else if (ft_words_count(map_line, ' ') != dt->c.size[0])
			exit(1);
		dt->c.size[1]++;
		if (!head)
			head = ft_lstnew(map_line, ft_strlen(map_line));
		else
			ft_lstaddlast(&head, ft_lstnew(map_line, ft_strlen(map_line) + 1));
		free(map_line);
	}
	close(fd_init[0]);
	dt->c.size[2] = dt->c.size[0] * dt->c.size[1];
	points_dt = NULL;
	return (string_to_coord(head, dt, -1, points_dt));
}
