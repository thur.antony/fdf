#include "fdf.h"

static void			angl_change(int key, t_data *dt)
{
	if (key == 113)
		dt->anglx += 0.1;
	else if (key == 119)
		dt->anglx -= 0.1;
	else if (key == 97)
		dt->angly += 0.1;
	else if (key == 115)
		dt->angly -= 0.1;
	else if (key == 122)
		dt->anglz += 0.1;
	else if (key == 120)
		dt->anglz -= 0.1;
}

static void			zoom_change(int key, t_data *dt)
{
	if (key == 61)
		dt->zoom += 2;
	else if (key == 45 && dt->zoom > 2)
		dt->zoom -= 2;
}

static void			offset_change(int key, t_data *dt)
{
	if (key == 65362)
		dt->offset_y -= 5;
	else if (key == 65364)
		dt->offset_y += 5;
	else if (key == 65361)
		dt->offset_x -= 5;
	else if (key == 65363)
		dt->offset_x += 5;
}

int					manipula_eos(int key, t_data *dt)
{
	if (key == 61 || key == 45)
		zoom_change(key, dt);
	else if (key > 65360 && key < 65365)
		offset_change(key, dt);
	else if (key > 96 && key < 123)
		angl_change(key, dt);
	else if (key == 53)
	{
		memory_cleaner(dt);
		exit(1);
	}
	make_picture(dt);
	return (1);
}
