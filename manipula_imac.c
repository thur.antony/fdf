/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manipula_imac.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/25 14:12:55 by avykhova          #+#    #+#             */
/*   Updated: 2018/04/25 14:12:57 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void			angl_change(int key, t_data *dt)
{
	if (key == 89)
		dt->anglx += 0.1;
	else if (key == 92)
		dt->anglx -= 0.1;
	else if (key == 86)
		dt->angly += 0.1;
	else if (key == 88)
		dt->angly -= 0.1;
	else if (key == 83)
		dt->anglz += 0.1;
	else if (key == 85)
		dt->anglz -= 0.1;
	else if (key == 69)
		dt->zoom_z += 2;
	else if (key == 78)
		dt->zoom_z -= 2;
}

static void			zoom_change(int key, t_data *dt)
{
	if (key == 24)
		dt->zoom += 2;
	else if (key == 27 && dt->zoom > 2)
		dt->zoom -= 2;
}

static void			offset_change(int key, t_data *dt)
{
	if (key == 126)
		dt->offset_y -= 5;
	else if (key == 125)
		dt->offset_y += 5;
	else if (key == 123)
		dt->offset_x -= 5;
	else if (key == 124)
		dt->offset_x += 5;
}

int					manipula_imac(int key, t_data *dt)
{
	if (key == 24 || key == 27)
		zoom_change(key, dt);
	else if (key > 122 && key < 127)
		offset_change(key, dt);
	else if (key > 68 && key < 93)
		angl_change(key, dt);
	else if (key == 53)
	{
		memory_cleaner(dt);
		exit(1);
	}
	make_picture(dt);
	return (1);
}
