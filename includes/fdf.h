/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 14:41:09 by avykhova          #+#    #+#             */
/*   Updated: 2018/03/26 14:41:10 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "fdf.h"
# include "get_next_line.h"
//# include <mlx.h>
# include "../libmlx/mlx.h"
# include <math.h>
# include <fcntl.h>

# define WIN_WIDTH 1200
# define WIN_HEIGHT 700
# define WIN_W WIN_WIDTH - 4;
# define WIN_H WIN_HEIGHT - 4;
# define IMG_LEN WIN_WIDTH * WIN_HEIGHT  * 4
# define R_1(argc) if (argc != 2) return (1);
# define WHITE 16777215

/*
**	HELPER STRUCTS
**	----------------------------------------------------------------
*/
typedef struct		s_delta
{
	int				x;
	int				y;
	int				abs_x;
	int				xx;
	int				yy;
}					t_delta;

/*
**	MAIN STRUCTS
**	----------------------------------------------------------------
*/

typedef struct		s_coord
{
	int				x;
	int				y;
	int				z;
	int				z_init;
	int				color;
}					t_coord;

/*
**	x:size[0] y:size[1] all:size[2]
*/
typedef struct		s_coord_arr
{
	t_coord			*all;
	int				size[3];
}					t_coord_arr;

typedef struct		s_data
{
	char			*name;

	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	char			*image;
	int				bpp;
	int				size_line;
	int				end;

	int				zoom;
	int				zoom_z;
	int				offset_x;
	int				offset_y;

	double			anglx;
	double			angly;
	double			anglz;

	int				z_max;
	int				step;

	t_coord_arr		c;
	t_coord_arr		w;
}					t_data;

/*
**	FUNCTIONALITY
**	----------------------------------------------------------------
*/

void				map_extractor(char *s, t_data *dt);

int					manipula_imac(int key, t_data *dt);
int					manipula_eos(int key, t_data *dt);

void				line(t_coord a, t_coord b, t_data *dt);
void				make_grid(t_data *dt);
void				make_picture(t_data *dt);
void				memory_cleaner(t_data *dt);

/*
** HELPERS
** -----------------------------------------------------------------
*/

void				add_offset_helper(t_data *dt);
void				string_put(t_data *dt);

#endif
