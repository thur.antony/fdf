/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 14:40:44 by avykhova          #+#    #+#             */
/*   Updated: 2018/03/26 14:40:46 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int				main(int argc, char **argv)
{
	t_data		dt;

	R_1(argc);
	dt.mlx_ptr = mlx_init();
	dt.win_ptr = mlx_new_window(dt.mlx_ptr, WIN_WIDTH, WIN_HEIGHT, argv[1]);
	dt.img_ptr = mlx_new_image(dt.mlx_ptr, WIN_WIDTH, WIN_HEIGHT);
	dt.image = mlx_get_data_addr(dt.img_ptr, &dt.bpp, &dt.size_line, &dt.end);
	dt.name = argv[1];
	dt.c.size[1] = 0;
	dt.bpp /= 8;
	dt.zoom = 3;
	dt.zoom_z = 10;
	dt.anglx = 0;
	dt.angly = 0;
	dt.anglz = 0;
	dt.z_max = 0;
	dt.offset_x = WIN_WIDTH / 2;
	dt.offset_y = WIN_HEIGHT / 2;
	map_extractor(argv[1], &dt);
	if (dt.z_max != 0)
		dt.step = 160 / dt.z_max;
	make_picture(&dt);
	mlx_hook(dt.win_ptr, 2, 5, manipula_imac, &dt);
	//mlx_hook(dt.win_ptr, 2, 5, manipula_eos, &dt);
	mlx_loop(dt.mlx_ptr);
	return (0);
}
